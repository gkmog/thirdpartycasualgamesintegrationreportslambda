package com.gamezy.CasualGamesReports.db;

import com.gamezy.CasualGamesReports.pojo.Vendor;
import com.gamezy.CasualGamesReports.utils.ExcelWriterUtil;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.joda.time.LocalDate;

import java.sql.*;

@Slf4j
public class RetentionReportQuery extends Query {

  private final static String query = "select\n" +
      "\ta.played_dt,\n" +
      "\tb.first_played_dt ,\n" +
      "\tdatediff(\n" +
      "\tb.first_played_dt,\n" +
      "\ta.played_dt) as df,\n" +
      "    count(1) as count\n" +
      "from\n" +
      "\t(\n" +
      "\tselect\n" +
      "\t\tuser_id,\n" +
      "\t\tdate(created_at) as played_dt\n" +
      "\tfrom\n" +
      "\t\tgamezy.fct_battle_account\n" +
      "\twhere\n" +
      "\t\tstatus = 5\n" +
      "and withdrawable_balance + deposit_balance + instant_cash_balance > 0 \n"
      +
      "                and created_at >= ? \n" +
      "and game_id in (?)\n" +
      "\tgroup by\n" +
      "\t\t1,\n" +
      "\t\t2) a\n" +
      "join (\n" +
      "\tselect\n" +
      "\t\tuser_id,\n" +
      "\t\tdate(min(created_at)) as first_played_dt\n" +
      "\tfrom\n" +
      "\t\tgamezy.fct_battle_account\n" +
      "\twhere\n" +
      "\t\tstatus = 5  \n" +
      "and withdrawable_balance + deposit_balance + instant_cash_balance > 0 \n"
      +
      "and game_id in (?)\n" +
      "\tgroup by\n" +
      "\t\t1\n" +
      "        having first_played_dt >= ?) b on\n" +
      "\ta.user_id = b.user_id\n" +
      "group by 3 order by 3 desc";

  private final static String startDateQuery =
      "SELECT created_at FROM bonusandfund.game_partners " +
          "WHERE partner_name = ? and game_id = ?;";

  private PreparedStatement statementToGetCreatedAt;

  public RetentionReportQuery() {
    log.info("Creating Connection for Retention Report");
    try {
      statement = connection.prepareStatement(query);
      statementToGetCreatedAt = connection.prepareStatement(startDateQuery);
    } catch (SQLException e) {
      log.error(e.getMessage());
    }
  }

  public ResultSet getResultSet(final String gameId, final LocalDate date)
      throws SQLException {
    final long startTime = System.currentTimeMillis();
    statement.setString(1, date.toString("yyyy-MM-dd"));
    statement.setString(2, gameId);
    statement.setString(3, gameId);
    statement.setString(4, date.toString("yyyy-MM-dd"));
    ResultSet rs = statement.executeQuery();
    log.debug("RetentionReportQuery: took {}ms",
        System.currentTimeMillis() - startTime);
    return rs;
  }

  public LocalDate getResultSetDate(final String gameId,
      final String vendorName) throws SQLException {
    log.info("vendor info {}", vendorName);
    final long startTime = System.currentTimeMillis();
    statementToGetCreatedAt.setString(1, vendorName);
    statementToGetCreatedAt.setString(2, gameId);
    ResultSet rs = statementToGetCreatedAt.executeQuery();
    log.info("RetentionReportQuery took {}ms",
        System.currentTimeMillis() - startTime);
    Date start = null;
    while (rs.next()) {
      start = rs.getDate("created_at");
    }
    assert start != null;
    return new LocalDate(start.getTime());
  }

  public static LocalDate getCreatedAtOrMonthStartDate(
      @NonNull final LocalDate createdAt, @NonNull final LocalDate yesterday) {
    LocalDate monthStart = yesterday.withDayOfMonth(1);
    if (createdAt.isBefore(monthStart)) {
      return monthStart;
    }
    return createdAt;
  }

  @Override
  public void writeToSheet(@NonNull final ExcelWriterUtil.ExcelSheet sheet,
      @NonNull final String yesterdayString,
      @NonNull final Vendor vendor) throws SQLException {
    final LocalDate yesterday = new LocalDate(yesterdayString);
    LocalDate startDate = getResultSetDate(vendor.getGameID(),
        vendor.getName());
    startDate = getCreatedAtOrMonthStartDate(startDate, yesterday);

    int count = 0;
    for (LocalDate date = yesterday; date.isAfter(yesterday.minusDays((30))) &&
        (date.isAfter(startDate) || date.isEqual(startDate));
        date = date.minusDays(1)) {
      log.info("today " + date);
      Row row = sheet.getRow(sheet.getRowFromDate(date.toString()));
      ResultSet rs = getResultSet(vendor.getGameID(), date);
      float retention = calculateRetentionPercentMain(rs, count);
      if (count == 0) {
        sheet.writeRowOnDate(date.toString(), retention);
        count++;
        continue;
      }

      Cell cell = row.createCell(count + 1);
      cell.setCellValue(retention);
      count++;
    }
  }

  public static float calculateRetentionPercentMain(ResultSet rs, int day) {
    int count = 0;
    int D0 = 0;
    try {
      log.info("calculated here for " + " day " + day);
      while (rs.next()) {
        if (count == 0 && day == count) {
          log.info("calculated here 1" + rs.getInt("count") + " day " + day);
          return 100;
        }
        if (count == 0) {
          log.info("calculated here 2 " + rs.getInt("count") + " day " + day);
          D0 = rs.getInt("count");
        }
        if (count == day) {
          log.info("calculated here 3" + rs.getInt("count") + " day " + day);
          int Dd = rs.getInt("count");
          if (Dd == 0) {
            return 0;
          }
          return (float) Dd / D0;
        }
        count++;
      }
    } catch (SQLException | NullPointerException e) {
      log.error("Error with calculation of retention percentage", e);
    }
    return 0;
  }

  public static void main(String[] args) {
    String s = getCreatedAtOrMonthStartDate(new LocalDate("2020-07-08"),
        new LocalDate("2020-08-20")).toString();
    System.out.println(s);
  }
}
