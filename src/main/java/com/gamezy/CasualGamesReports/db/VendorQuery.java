package com.gamezy.CasualGamesReports.db;

import com.gamezy.CasualGamesReports.pojo.Vendor;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class VendorQuery {

  static Connection connection;
  static PreparedStatement statement;
  final static String query = "select * from bonusandfund.game_partners";
  static List<Vendor> vendors;

  public static List<Vendor> getVendors() {
    if (vendors == null) {
      try {
        fetchVendorsFromDB();
      } catch (SQLException | NullPointerException e) {
        log.error("Could not fetch Vendors", e);
        return new ArrayList<>();
      }
    }
    return vendors;
  }

  private static void fetchVendorsFromDB()
      throws SQLException, NullPointerException {
    log.info("Creating Connection for fetching Vendors");
    connection = ConnectionManager.getBFMConnection();
    statement = connection.prepareStatement(query);

    vendors = new ArrayList<>();
    final ResultSet rs = statement.executeQuery();
    while (rs.next()) {
      Vendor vendor = Vendor.builder().
          name(rs.getString("partner_name")).
          gameID(rs.getString("game_id")).
          commissionPercentage(rs.getDouble("commission_percentage")).
          build();
      log.info("Build report for {}", vendor);
      vendors.add(vendor);
    }
  }
}
