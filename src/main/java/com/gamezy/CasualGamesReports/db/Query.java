package com.gamezy.CasualGamesReports.db;

import com.gamezy.CasualGamesReports.pojo.Vendor;
import com.gamezy.CasualGamesReports.utils.ExcelWriterUtil;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.LocalDate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Slf4j
public abstract class Query {
    final Connection connection = (new ConnectionManager()).getConnection();
    PreparedStatement statement;

    public abstract void writeToSheet(ExcelWriterUtil.ExcelSheet sheet, String yesterday, Vendor vendor)
            throws SQLException, IllegalArgumentException;
}
