package com.gamezy.CasualGamesReports.db;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.sql.*;
import java.util.Properties;

@Slf4j
public class ConnectionManager {

  private static final String CLASS_NAME = "com.mysql.cj.jdbc.Driver";
  private static Connection connection;
  private static String DB_URL, DB_USERNAME, DB_PASSWORD;

  public static void connect() {
    try {
      Class.forName(CLASS_NAME);
      log.info("Using ENV {} Connecting to the database: {}",
          System.getenv("ENV"), DB_URL);
      DriverManager.setLoginTimeout(60);
      connection = DriverManager.getConnection(DB_URL, DB_USERNAME,
          DB_PASSWORD);
    } catch (SQLException | ClassNotFoundException e) {
      log.error("Could not connect to DB", e);
    }
  }

  private static void loadCredentials() {
    Properties prop = new Properties();
    try {
      prop.load(
          ConnectionManager.class.getResourceAsStream("/config.properties"));
      final String env = System.getenv("ENV");
      if (env != null && env.equals("PROD")) {
        DB_URL = prop.getProperty("PROD_DB_URL");
        DB_USERNAME = prop.getProperty("PROD_DB_USERNAME");
        DB_PASSWORD = prop.getProperty("PROD_DB_PASSWORD");
      } else if (env != null && env.equals("STAGE")) {
        DB_URL = prop.getProperty("STAGE_DB_URL");
        DB_USERNAME = prop.getProperty("STAGE_DB_USERNAME");
        DB_PASSWORD = prop.getProperty("STAGE_DB_PASSWORD");
      } else {
        DB_URL = prop.getProperty("LOCAL_DB_URL");
        DB_USERNAME = prop.getProperty("LOCAL_DB_USERNAME");
        DB_PASSWORD = prop.getProperty("LOCAL_DB_PASSWORD");
      }
    } catch (IOException e) {
      log.error("Could not load DB credentials", e);
    }
  }

  public static Connection getConnection() {
    loadCredentials();
    connect();
    return connection;
  }

  public static Connection getBFMConnection() {
    Properties prop = new Properties();
    try {
      prop.load(
          ConnectionManager.class.getResourceAsStream("/config.properties"));
      final String env = System.getenv("ENV");
      if (env != null && env.equals("PROD")) {
        DB_URL = prop.getProperty("PROD_BFM_URL");
        DB_USERNAME = prop.getProperty("PROD_BFM_USERNAME");
        DB_PASSWORD = prop.getProperty("PROD_BFM_PASSWORD");
      } else if (env != null && env.equals("STAGE")) {
        DB_URL = prop.getProperty("STAGE_DB_URL");
        DB_USERNAME = prop.getProperty("STAGE_DB_USERNAME");
        DB_PASSWORD = prop.getProperty("STAGE_DB_PASSWORD");
      } else {
        DB_URL = prop.getProperty("LOCAL_DB_URL");
        DB_USERNAME = prop.getProperty("LOCAL_DB_USERNAME");
        DB_PASSWORD = prop.getProperty("LOCAL_DB_PASSWORD");
      }
    } catch (IOException e) {
      log.error("Could not load DB credentials", e);
    }
    try {
      Class.forName(CLASS_NAME);
      log.info("Using ENV {} Connecting to the database: {}",
          System.getenv("ENV"), DB_URL);
      DriverManager.setLoginTimeout(60);
      connection = DriverManager.getConnection(DB_URL, DB_USERNAME,
          DB_PASSWORD);
    } catch (SQLException | ClassNotFoundException e) {
      log.error("Could not connect to DB", e);
    }
    return connection;
  }
  public static void main(String[] args) {
    getConnection();
  }
}

