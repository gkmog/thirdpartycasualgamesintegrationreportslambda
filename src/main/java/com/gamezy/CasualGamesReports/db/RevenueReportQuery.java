package com.gamezy.CasualGamesReports.db;

import com.gamezy.CasualGamesReports.pojo.Vendor;
import com.gamezy.CasualGamesReports.utils.DateUtil;
import com.gamezy.CasualGamesReports.utils.ExcelWriterUtil;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.LocalDate;

import java.sql.*;

@Slf4j
public class RevenueReportQuery extends Query {
    private final static String query = "select sum(partner_commission) "
             + "as Net_Commission from gamezy.fct_battle_account as a where status = 5 and game_id = ? and "
             + "created_at >= ? and created_at <= ?;";

    public RevenueReportQuery() {
        log.info("Creating Connection for Revenue Report");
        try {
            statement = connection.prepareStatement(query);
        } catch (SQLException e) {
            log.error("Error while creating statement", e);
        }
    }

    public ResultSet getResultSet(@NonNull final String gameId, @NonNull final String yesterday)
            throws SQLException {
        final long startTime = System.currentTimeMillis();
        statement.setString(1, gameId);
        statement.setString(2,  yesterday + " 18:30:00");
        statement.setString(3, DateUtil.nextDay(yesterday) + " 18:29:59");
        final ResultSet rs = statement.executeQuery();
        log.info("RevenueReportQuery took {}ms", System.currentTimeMillis() - startTime);
        return rs;
    }

    @Override
    public void writeToSheet(@NonNull final ExcelWriterUtil.ExcelSheet sheet, @NonNull final String yesterday,
                             @NonNull final Vendor vendor) throws SQLException, IllegalArgumentException {
        final ResultSet rs = getResultSet(vendor.getGameID(), yesterday);
        while (rs.next()) {
            sheet.writeRowOnDate(DateUtil.nextDay(yesterday),
                    vendor.getGameID(), rs.getDouble("Net_Commission"));
        }
    }

    public static void main(String[] args) {
        log.info(LocalDate.now().toString());
    }
}
