package com.gamezy.CasualGamesReports.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.gamezy.CasualGamesReports.db.*;
import com.gamezy.CasualGamesReports.pojo.Report;
import com.gamezy.CasualGamesReports.service.SlackService;
import com.gamezy.CasualGamesReports.utils.DateUtil;
import java.util.concurrent.atomic.AtomicReference;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.joda.time.LocalDate;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.gamezy.CasualGamesReports.constants.ExcelConstants.*;
import static com.gamezy.CasualGamesReports.constants.ExcelConstants.RETENTION_REPORTS_HEADER;
import static com.gamezy.CasualGamesReports.constants.GDriveConstants.*;
import static com.gamezy.CasualGamesReports.constants.GDriveConstants.RETENTION_REPORT_PATH;

@Slf4j
public class CustomReportHandler implements RequestHandler<String, Void> {

  static AtomicReference<Integer> generatedReports;
  final static SlackService slack;

  static {
    slack = new SlackService();
    generatedReports = new AtomicReference<>(0);
  }

  @Override
  // input: "2020-07-01,Revenue-Report" -> for 2020-06-30 18:30:00 UTC to 2020-07-01 18:29:59 UTC
  public Void handleRequest(String input, Context context) {
    slack.sendMessage("CustomReportHandler triggered", false);
    final StopWatch stopWatch = new StopWatch();
    stopWatch.start();
    if (StringUtils.isBlank(input)) {
      return null;
    }
    List<Report> reports = new ArrayList<>();
    LocalDate yesterday = null;
    for (String s : input.split(",")) {
        if (StringUtils.isBlank(s)) {
            continue;
        }
      switch (s) {
        case REVENUE_REPORT_PATH:
          reports.add(new Report(REVENUE_REPORT_PATH, REVENUE_REPORTS_HEADER,
              new RevenueReportQuery()));
          log.info("Generate report {}", REVENUE_REPORT_PATH);
          break;
        case ACTIVE_USERS_REPORT_PATH:
          reports.add(new Report(ACTIVE_USERS_REPORT_PATH, DAU_REPORTS_HEADER,
              new ActiveUsersQuery()));
          log.info("Generate report {}", ACTIVE_USERS_REPORT_PATH);
          break;
        case BATTLES_PLAYED_REPORT_PATH:
          reports.add(new Report(BATTLES_PLAYED_REPORT_PATH,
              TOTAL_BATTLES_PLAYED_REPORTS_HEADER, new BattlesPlayedQuery()));
          log.info("Generate report {}", BATTLES_PLAYED_REPORT_PATH);
          break;
        case RETENTION_REPORT_PATH:
          reports.add(
              new Report(RETENTION_REPORT_PATH, RETENTION_REPORTS_HEADER,
                  new RetentionReportQuery()));
          log.info("Generate report {}", RETENTION_REPORT_PATH);
          break;
        default:
          try {
            yesterday = LocalDate.parse(s);
          } catch (Exception e) {
            log.error("Not a date or report {}", s, e);
          }
      }
    }

    if (yesterday == null) {
      return null;
    }
    final Calendar cal = Calendar.getInstance();
    cal.setTime(yesterday.toDate());
    slack.sendMessage("for " + yesterday.toString("yyyy-MM-dd") +
        " 18:30:00 UTC to " + DateUtil.nextDay(yesterday.toString("yyyy-MM-dd"))
        + " 18:29:59 UTC", false);

    generatedReports.set(0);
    final String monthString = LocalDate.fromCalendarFields(cal)
        .toString("MMM", Locale.ENGLISH);
    final String yearString = LocalDate.fromCalendarFields(cal)
        .toString("YYYY", Locale.ENGLISH);
    try {
      final Connection connection = ConnectionManager.getConnection();
      if (!connection.isClosed()) {
        LocalDate finalYesterday = yesterday;
        reports.stream().parallel().forEach(report -> {
          int count = DailyReportHandler.generateReport(report, monthString,
              yearString,
              finalYesterday);
          generatedReports.set(generatedReports.get() + count);
        });
        connection.close();
      }
    } catch (SQLException e) {
      log.error("Can not close SQL connection", e);
    }
    slack.sendMessage(
        "Generated " + generatedReports + " reports in " + stopWatch, false);
    return null;
  }

  public static void main(String[] args) {
    String inp =
        "2021-08-02," + REVENUE_REPORT_PATH + "," + ACTIVE_USERS_REPORT_PATH
            + "," + BATTLES_PLAYED_REPORT_PATH;
    new CustomReportHandler().handleRequest(inp, null);
  }
}