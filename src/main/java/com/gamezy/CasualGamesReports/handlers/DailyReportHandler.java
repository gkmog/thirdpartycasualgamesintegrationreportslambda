package com.gamezy.CasualGamesReports.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.gamezy.CasualGamesReports.db.*;
import com.gamezy.CasualGamesReports.pojo.Vendor;
import com.gamezy.CasualGamesReports.service.SlackService;
import com.gamezy.CasualGamesReports.utils.DateUtil;
import com.gamezy.CasualGamesReports.utils.ExcelWriterUtil;
import com.gamezy.CasualGamesReports.utils.ExcelWriterUtil.ExcelSheet;
import com.gamezy.CasualGamesReports.utils.GDriveUtil;
import com.gamezy.CasualGamesReports.pojo.Report;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.joda.time.LocalDate;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.gamezy.CasualGamesReports.db.VendorQuery.getVendors;
import static com.gamezy.CasualGamesReports.constants.ExcelConstants.*;
import static com.gamezy.CasualGamesReports.constants.GDriveConstants.*;

@Slf4j
public class DailyReportHandler implements RequestHandler<Void, Void> {

  static AtomicReference<Integer> generatedReports;
  static final List<Vendor> vendors;
  static final GDriveUtil drive;
  static final SlackService slack;
  static final String rootFolder;
  static final List<Report> reports = new ArrayList<>();
  static final String monthString, yearString;
  static final LocalDate yesterday;

  static {
    generatedReports = new AtomicReference<>(0);
    slack = new SlackService();
    drive = new GDriveUtil();
    rootFolder = System.getenv("ENV") != null ? System.getenv("ENV") : "LOCAL";
    vendors = getVendors();
        reports.add(new Report(REVENUE_REPORT_PATH, REVENUE_REPORTS_HEADER, new RevenueReportQuery()));
        reports.add(new Report(BATTLES_PLAYED_REPORT_PATH, TOTAL_BATTLES_PLAYED_REPORTS_HEADER, new BattlesPlayedQuery()));
        reports.add(new Report(ACTIVE_USERS_REPORT_PATH, DAU_REPORTS_HEADER, new ActiveUsersQuery()));
//    reports.add(new Report(RETENTION_REPORT_PATH, RETENTION_REPORTS_HEADER,
//        new RetentionReportQuery()));

//    String runForDate = "2021-09-01";//
    Calendar cal = Calendar.getInstance();
    Calendar cal2 = Calendar.getInstance();
//    cal.setTime((new LocalDate(runForDate)).toDate());//
//    cal2.setTime((new LocalDate(runForDate)).toDate());//
    cal.add(Calendar.DATE, -1);
    yesterday = LocalDate.fromCalendarFields(cal);
    monthString = LocalDate.fromCalendarFields(cal2)
        .toString("MMM", Locale.ENGLISH);
    yearString = LocalDate.fromCalendarFields(cal2)
        .toString("YYYY", Locale.ENGLISH);
  }

  @Override
  public Void handleRequest(Void unused, Context context) {
    final StopWatch stopWatch = new StopWatch();
    stopWatch.start();
    generatedReports.set(0);
    slack.sendMessage(
        "DailyReportHandler triggered for " + yesterday.toString("yyyy-MM-dd") +
            " 18:30:00 UTC to " + DateUtil.nextDay(yesterday.toString("yyyy-MM-dd"))
            + " 18:29:59 UTC", false);
    try {
      final Connection connection = ConnectionManager.getConnection();
      if (!connection.isClosed()) {
        reports.stream().parallel().forEach(report -> {
          int count = generateReport(report, monthString, yearString,
              yesterday);
          generatedReports.set(generatedReports.get() + count);
        });
        connection.close();
      }
    } catch (SQLException e) {
      log.error("Can not close SQL connection", e);
    }
    slack.sendMessage(
        "Generated " + generatedReports + " reports in " + stopWatch.toString(),
        false);
    return null;
  }

  static Integer generateReport(@NonNull final Report report,
      @NonNull final String monthString,
      @NonNull final String yearString, @NonNull LocalDate yesterday) {
    AtomicReference<Integer> successfulReports = new AtomicReference<>(0);
    final Query query = report.getQuery();
    vendors.stream().parallel().forEach(vendor -> {
      final String directoryPath =
          vendor.getName() + "/" + "Game_" + vendor.getGameID() + "/"
              + report.getPath();
      final String filename =
          "/tmp/Reports/" + directoryPath + "/" + yearString + ".xlsx";
      try {
        String fileId = drive.downloadFile(rootFolder, filename, directoryPath);
        final ExcelWriterUtil excelWriter = new ExcelWriterUtil(filename);
        final ExcelWriterUtil.ExcelSheet sheet = excelWriter.getSheet(
            monthString, report.getHeader());
        writeToSheet(yesterday, query, vendor, sheet);
        excelWriter.close();
        if (StringUtils.isNotBlank(fileId)) {
          drive.updateFile(fileId, MIME_TYPE_XLSX, filename);
        } else {
          drive.createFolderAndUploadFile(rootFolder, filename, directoryPath,
              MIME_TYPE_XLSX);
        }

        Files.delete(Paths.get(filename));
        successfulReports.set(successfulReports.get() + 1);
      } catch (IllegalArgumentException |
          IOException |
          InvalidFormatException |
          SQLException |
          NullPointerException e) {
        log.error("Dev {}:", vendor.getName(), e);
        slack.sendMessage(e.getMessage(), true);
      }
    });
    log.info("{} Completed", report.getPath());
    return successfulReports.get();
  }

  // synchronized so that only 1 call is made to DB at a time
  private static synchronized void writeToSheet(LocalDate yesterday,
      Query query,
      Vendor vendor, ExcelSheet sheet) throws SQLException {
    query.writeToSheet(sheet, yesterday.toString("yyyy-MM-dd"), vendor);
  }

  public static void main(String[] args) {
    new DailyReportHandler().handleRequest(null, null);
  }
}
