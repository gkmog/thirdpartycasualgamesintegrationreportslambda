package com.gamezy.CasualGamesReports.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import com.github.seratch.jslack.Slack;
import com.github.seratch.jslack.api.webhook.Payload;

@Slf4j
public class SlackService {

  private String urlSlackWebHook;
  private String botName = "LOCAL";

  public SlackService() {
    Properties prop = new Properties();
    try {
      prop.load(SlackService.class.getResourceAsStream("/config.properties"));
      urlSlackWebHook = prop.getProperty("SLACK.webhook");
      if (System.getenv("ENV") != null) {
        botName = System.getenv("ENV");
      }
    } catch (IOException e) {
      log.error("Could not load slack webhook", e);
    }
  }

  public void sendMessage(final String message, Boolean isError) {
    process((isError ? "<!channel> *ERROR OCCURRED:* " : "") + message);
  }

  private void process(@NonNull final String message) {
    Payload payload = Payload.builder()
        .channel("#third-party-casual-game-report-alerts")
        .username(botName)
        .iconEmoji(":rocket:")
        .text(message)
        .build();
    try {
      Slack.getInstance().send(urlSlackWebHook, payload);
    } catch (IOException e) {
      log.error("Slack could not send message!");
    }
  }
}
