package com.gamezy.CasualGamesReports.pojo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Vendor {
    String gameID, name;
    Double commissionPercentage;
}

