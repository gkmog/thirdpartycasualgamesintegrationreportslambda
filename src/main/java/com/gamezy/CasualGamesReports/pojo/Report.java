package com.gamezy.CasualGamesReports.pojo;

import com.gamezy.CasualGamesReports.db.Query;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Report {
    final String path;
    final String[] header;
    final Query query;
}
