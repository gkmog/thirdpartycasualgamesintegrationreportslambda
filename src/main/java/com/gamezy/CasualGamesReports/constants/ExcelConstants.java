package com.gamezy.CasualGamesReports.constants;

public class ExcelConstants {
    private ExcelConstants(){}

    public static final String[] REVENUE_REPORTS_HEADER = {"DATE (REPORT)", "Game ID", "Net Commission"};
    public static final String[] DAU_REPORTS_HEADER = {"DATE (REPORT)", "Game ID","Daily Active Users"};
    public static final String[] TOTAL_BATTLES_PLAYED_REPORTS_HEADER = {"DATE (REPORT)", "Game ID","Total Battles Played"};
    public static final String[] RETENTION_REPORTS_HEADER = {"DATE (REPORT)","D0","D1","D2","D3","D4","D5","D6",
            "D7","D8","D9","D10","D11","D12","D13","D14","D15","D16","D17","D18","D19","D20","D21","D22","D23","D24","D25",
            "D26","D27","D28","D29","D30"};
}
