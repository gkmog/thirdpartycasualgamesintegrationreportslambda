package com.gamezy.CasualGamesReports.constants;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

public class GDriveConstants {
    private GDriveConstants() {}

    public static final String MIME_TYPE_FOLDER = "application/vnd.google-apps.folder";
    public static final String MIME_TYPE_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static final String REVENUE_REPORT_PATH = "Revenue-Report";
    public static final String ACTIVE_USERS_REPORT_PATH = "Active-Users-Report";
    public static final String BATTLES_PLAYED_REPORT_PATH = "Battles-Played-Report";
    public static final String RETENTION_REPORT_PATH = "Retention-Report";

    public static final Map<String, String> driveRootFolderId =
            ImmutableMap.<String, String> builder()
                    .put("LOCAL", "1dwaShGD6WMWhjpZMXYVOimTHtdH50x72") // vibhor's drive folder
                    .put("STAGE", "1-OnMgrNI7KNL90y0ZH8oWeSzOgiJBTtU")
                    .put("PROD", "1Xd92tn92GwpV8igi5DqnMydc-z6u4yf5")
                    .build();
}
