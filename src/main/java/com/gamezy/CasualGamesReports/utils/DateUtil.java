package com.gamezy.CasualGamesReports.utils;

import java.util.HashMap;
import java.util.Map;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.LocalDate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@Slf4j
public class DateUtil {

  static final Calendar cal = Calendar.getInstance();
  static final DateFormat format = new SimpleDateFormat("yyyy-MM-dd",
      Locale.ENGLISH);
  static final Map<String, String> cache = new HashMap<>();

  public static String nextDay(@NonNull final String date) {
    String cached = cache.get(date);
    if (cached != null) {
      return cached;
    }
    try {
      final Date nextDate = format.parse(date);
      cal.setTime(nextDate);
      cal.add(Calendar.DATE, +1);
    } catch (ParseException e) {
      log.error("Can not parse date {}", date);
      return date;
    }
    cached = LocalDate.fromCalendarFields(cal).toString("yyyy-MM-dd");
    cache.put(date, cached);
    return cached;
  }

  public static void main(String[] args) {
    log.info("date {}", nextDay("2020-08-31"));
  }
}
