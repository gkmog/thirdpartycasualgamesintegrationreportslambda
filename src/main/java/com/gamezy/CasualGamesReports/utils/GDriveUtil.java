package com.gamezy.CasualGamesReports.utils;

import static com.gamezy.CasualGamesReports.constants.GDriveConstants.MIME_TYPE_FOLDER;
import static com.gamezy.CasualGamesReports.constants.GDriveConstants.MIME_TYPE_XLSX;
import static com.gamezy.CasualGamesReports.constants.GDriveConstants.driveRootFolderId;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class GDriveUtil {

  private static final String APPLICATION_NAME = "CasualGamesReportsUtil";
  private static final String SLASH = "/";
  private static final String EMPTY_STRING = "";
  private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
  private static Drive service;
  private static final List<String> SCOPES = new ArrayList<>(DriveScopes.all());
  private static final String CREDENTIALS_FILE_PATH = "/cred.json";

  private static Credential getCredentials() throws IOException {
    return GoogleCredential.fromStream(
        Objects.requireNonNull(
            GDriveUtil.class.getResourceAsStream(CREDENTIALS_FILE_PATH))
    ).createScoped(SCOPES);
  }

  public GDriveUtil() {
    try {
      final NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
      service = new Drive.Builder(httpTransport, JSON_FACTORY, getCredentials())
          .setHttpRequestInitializer(httpRequest -> {
            getCredentials().initialize(httpRequest);
            httpRequest.setConnectTimeout(60000);  // 1 minute connect timeout
            httpRequest.setReadTimeout(60000);  // 1 minute read timeout
          }).setApplicationName(APPLICATION_NAME).build();
    } catch (Exception e) {
      log.error("Error while configuring google drive: {}", e.getMessage());
    }
  }

  private String doesResourceExists(@NonNull final String parentId,
      @NonNull final String fileName,
      @NonNull final Boolean isFolder) throws IOException {
    String pageToken = null;
    String fileId = null;
    final List<String> parentList = new ArrayList<>(
        Collections.singletonList(parentId));
    do {
      final FileList result = service.files().list()
          .setQ("name='" + fileName + "'" +
              (isFolder ? "and mimeType = '" + MIME_TYPE_FOLDER + "' " : "") +
              "and trashed = false")
          .setSpaces("drive")
          .setFields("nextPageToken, files(id, name, parents)")
          .setPageToken(pageToken).execute();

      if (result != null && !result.isEmpty()) {
        for (final File file : result.getFiles()) {
          if (fileName.equals(file.getName()) && CollectionUtils.isNotEmpty(
              file.getParents())
              && CollectionUtils.isEqualCollection(parentList,
              file.getParents())) {
            fileId = file.getId();
            break;
          }
        }
        pageToken = result.getNextPageToken();
      }
    } while (pageToken != null);
    return fileId;
  }

  private String createFolder(@NonNull final String parentId,
      @NonNull final String folderName) throws IOException {
    final File fileMetadata = new File();
    fileMetadata.setName(folderName);
    fileMetadata.setParents(
        new ArrayList<>(Collections.singletonList(parentId)));
    fileMetadata.setMimeType(MIME_TYPE_FOLDER);

    final String folderId = service.files().create(fileMetadata).setFields("id")
        .execute().getId();
    log.debug(
        "Successfully created folder in google drive for folderName: {} - parentId: {}",
        folderName, parentId);
    return folderId;
  }

  private String createOrTraverseFolderIfNotExist(@NonNull String parentId,
      @NonNull final String driveDirectoryPath)
      throws IOException {
    for (final String folder : driveDirectoryPath.split(SLASH)) {
      if (StringUtils.isNotBlank(folder)) {
        final String folderId = doesResourceExists(parentId, folder, true);
        if (StringUtils.isBlank(folderId)) {
          parentId = createFolder(parentId, folder);
        } else {
          parentId = folderId;
        }
      }
    }
    return parentId;
  }

  public String uploadFile(@NonNull final String parentId,
      @NonNull final String mimeType,
      @NonNull final String path) throws IOException {
    final long startTime = System.currentTimeMillis();
    final String fileName = path.replaceAll(".*/", EMPTY_STRING);
    final File fileMetadata = new File();
    fileMetadata.setName(fileName);
    fileMetadata.setParents(
        new ArrayList<>(Collections.singletonList(parentId)));
    final java.io.File filePath = new java.io.File(path);
    final FileContent mediaContent = new FileContent(mimeType, filePath);

    File file = service.files().create(fileMetadata, mediaContent)
        .setFields("id").execute();
    log.info("File {} uploaded in {}ms", path,
        System.currentTimeMillis() - startTime);
    return file.getId();
  }

  public void updateFile(@NonNull final String fileId,
      @NonNull final String mimeType,
      @NonNull final String path) throws IOException {
    final long startTime = System.currentTimeMillis();
    final String fileName = path.replaceAll(".*/", EMPTY_STRING);
    final File fileMetadata = new File();
    fileMetadata.setName(fileName);
    final java.io.File fileContent = new java.io.File(path);
    final FileContent mediaContent = new FileContent(mimeType, fileContent);

    service.files().update(fileId, fileMetadata, mediaContent).execute();
    log.info("File {} updated in {}ms", path,
        System.currentTimeMillis() - startTime);
  }

  public void createFolderAndUploadFile(@NonNull final String rootFolder,
      @NonNull final String file,
      @NonNull final String driveDirectoryPath, @NonNull final String mimeType)
      throws IOException {
    final String parentId = createOrTraverseFolderIfNotExist(
        driveRootFolderId.get(rootFolder), driveDirectoryPath);

    final String fileName = file.replaceAll(".*/", EMPTY_STRING);
    final String fileId = doesResourceExists(parentId, fileName, false);

    if (StringUtils.isBlank(fileId)) {
      uploadFile(parentId, mimeType, file);
    } else {
      updateFile(fileId, mimeType, file);
    }
  }

  public void createFolderAndUploadFiles(@NonNull final String rootFolder,
      @NonNull final List<String> files,
      @NonNull final String driveDirectoryPath, @NonNull final String mimeType)
      throws IOException {
    final String parentId = createOrTraverseFolderIfNotExist(
        driveRootFolderId.get(rootFolder), driveDirectoryPath);
    for (String file : files) {
      final String fileName = file.replaceAll(".*/", EMPTY_STRING);
      final String fileId = doesResourceExists(parentId, fileName, false);
      if (StringUtils.isBlank(fileId)) {
        uploadFile(parentId, mimeType, file);
      } else {
        updateFile(fileId, mimeType, file);
      }
    }
  }

  public String downloadFile(@NonNull final String rootFolder,
      @NonNull final String fileName,
      @NonNull final String driveDirectoryPath) throws IOException {
    final long startTime = System.currentTimeMillis();
    final String parentId = createOrTraverseFolderIfNotExist(
        driveRootFolderId.get(rootFolder), driveDirectoryPath);
    final String fileId =
        doesResourceExists(parentId, fileName.replaceAll(".*/", EMPTY_STRING),
            false);

    if (StringUtils.isBlank(fileId)) {
      log.warn("File {} doest not exist", fileName);
      return null;
    }

    final java.io.File file = new java.io.File(fileName);
    if (fileName.contains("/")) {
      String dir = fileName.substring(0, fileName.lastIndexOf('/'));
      new java.io.File(dir).mkdirs();
    }

    final OutputStream outputStream = new FileOutputStream(file, false);
    service.files().get(fileId).executeMediaAndDownloadTo(outputStream);
    outputStream.close();

    log.info("File {} downloaded in {}ms", fileName,
        System.currentTimeMillis() - startTime);
    return fileId;
  }

  public static void main(String... args) {
    String path = "/Users/vibhorrawal/Downloads/CasualGamesReports/test.xlsx";
    GDriveUtil drive = new GDriveUtil();
    List<String> list = new ArrayList<>(Collections.singletonList(path));
    try {
      log.info(drive.uploadFile(driveRootFolderId.get("vibhor"), MIME_TYPE_XLSX,
          "/Users/vibhorrawal/Downloads/CasualGamesReports/test.xlsx"));
    } catch (IOException e) {
      log.error("error ", e);
    }
  }
}