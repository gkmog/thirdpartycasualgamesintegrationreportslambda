package com.gamezy.CasualGamesReports.utils;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

@Slf4j
public class ExcelWriterUtil implements Closeable {

  private final String xlsxFileName;
  private final Workbook workbook;
  private final CellStyle bigDecimalCellStyle;

  public ExcelWriterUtil(@NonNull final String xlsxFileName)
      throws IOException, InvalidFormatException {
    this.xlsxFileName = xlsxFileName;
    if (xlsxFileName.contains("/")) {
      String dir = xlsxFileName.substring(0, xlsxFileName.lastIndexOf('/'));
      new File(dir).mkdirs();
    }
    this.workbook = new File(xlsxFileName).exists() ?
        WorkbookFactory.create(new FileInputStream(xlsxFileName))
        : new XSSFWorkbook();
    this.bigDecimalCellStyle = workbook.createCellStyle();
    this.bigDecimalCellStyle.setDataFormat(
        workbook.createDataFormat().getFormat("#.##"));
    log.debug("Workbook {} instantiated", xlsxFileName);
  }

  public ExcelSheet getSheet(String sheetName, String[] header) {
    return new ExcelSheet(sheetName, header);
  }

  @Override
  public void close() throws IOException {
    try (final FileOutputStream out = new FileOutputStream(
        new File(xlsxFileName))) {
      workbook.write(out);
      workbook.close();
      log.debug("Workbook {} closed", xlsxFileName);
    } catch (Exception e) {
      log.error(e.getMessage());
    }
  }

  public class ExcelSheet {

    private final String sheetName;
    private final Sheet sheet;
    private int rowCount;

    public ExcelSheet(@NonNull final String sheetName,
        @NonNull final String[] header) {
      this.sheetName = sheetName;
      if (workbook.getSheetIndex(sheetName) == -1) {
        sheet = workbook.createSheet(sheetName);
        setHeader(header);
        for (int i = 0; i < header.length; i++) {
          sheet.autoSizeColumn(i);
        }
        log.debug("New Sheet {} created in {}", sheetName, xlsxFileName);
      } else {
        sheet = workbook.getSheet(sheetName);
      }
      rowCount = sheet.getLastRowNum() + 1;
      log.debug("{} Rows exist in {}/{}", rowCount, xlsxFileName, sheetName);
    }

    private void setHeader(@NonNull final String[] header) {
      Font headerFont = workbook.createFont();
      headerFont.setBold(true);
      headerFont.setFontHeightInPoints((short) 12);
      headerFont.setColor(IndexedColors.BLACK.index);

      CellStyle headerStyle = workbook.createCellStyle();
      headerStyle.setFont(headerFont);

      Row headerRow = sheet.createRow(0);
      for (int i = 0; i < header.length; i++) {
        Cell cell = headerRow.createCell(i);
        cell.setCellValue(header[i]);
        cell.setCellStyle(headerStyle);
      }
      sheet.createFreezePane(0, 1);
    }

    private synchronized void writeRowUtil(@NonNull Row row,
        final Object... rowElements) {
      int cellCount = 0;
      for (final Object obj : rowElements) {
        Cell cell = row.createCell(cellCount++);
        if (obj instanceof Date) {
          cell.setCellValue((Date) obj);
        } else if (obj instanceof Boolean) {
          cell.setCellValue((Boolean) obj);
        } else if (obj instanceof String) {
          cell.setCellValue((String) obj);
        } else if (obj instanceof Double) {
          cell.setCellValue((Double) obj);
        } else if (obj instanceof Integer) {
          cell.setCellValue((Integer) obj);
        } else if (obj instanceof Long) {
          cell.setCellValue((Long) obj);
        } else if (obj instanceof Float) {
          cell.setCellValue((Float) obj);
        } else if (obj instanceof BigDecimal) {
          double data = ((BigDecimal) obj).doubleValue();
          cell.setCellStyle(bigDecimalCellStyle);
          cell.setCellValue(data);
        }
      }
      log.debug("{} Columns written in {}/{}", rowElements.length, xlsxFileName,
          sheetName);
    }

    public Integer getRowFromDate(final String date)
        throws IllegalArgumentException {
      if (date.length() < 2) {
        throw new IllegalArgumentException("Date expected");
      }
      return Integer.valueOf(date.substring(date.length() - 2));
    }

    public synchronized void writeRow(final Object... rowElements) {
      Row row = sheet.createRow(rowCount++);
      writeRowUtil(row, rowElements);
    }

    public synchronized void writeRowOnDate(final Object... rowElements)
        throws IllegalArgumentException {
      Row row = sheet.createRow(getRowFromDate((String) rowElements[0]));
      writeRowUtil(row, rowElements);
    }

    public int getRowCount() {
      return rowCount;
    }

    public void deleteRow(final int rowNumber) {
      sheet.removeRow(sheet.getRow(rowNumber));
    }

    public Row getRow(final int rowNumber) {
      Row row = sheet.getRow(rowNumber);
        if (row == null) {
            row = sheet.createRow(rowNumber);
        }
      return row;
    }
  }

  public static void main(String[] args) throws Exception {
    ExcelWriterUtil excelWriter = new ExcelWriterUtil(
        "/Users/vibhorrawal/Downloads/CasualGamesReports/test2.xlsx");
    String[] header = {"head 1", "head 2", "head 3"};
    ExcelSheet sheet = excelWriter.getSheet("sheet one", header);
    sheet.getRow(1).toString();

    sheet.writeRow("Col1", "Col2", "Col3");
    final short lastCellNum1 = sheet.getRow(1).getLastCellNum();
    sheet.writeRow(BigDecimal.valueOf(45.456), 11, "cc");
    sheet.writeRow(BigDecimal.valueOf(25), 13, "cc1");
    sheet.writeRowOnDate("20", "1");
    final Integer rowFromDate = sheet.getRowFromDate("19");
    final short lastCellNum = sheet.getRow(rowFromDate).getLastCellNum();
    log.info("last {} {} {}", sheet.getRow(1).toString(), lastCellNum1,
        lastCellNum);
    excelWriter.close();
  }
}
